#!/bin/sh

docker run \
    -d \
    --name smtp_test \
    -p 30003:8025 \
    -e REDIS_URL=redis://172.17.0.1:16604/4 \
    registry.gitlab.com/public.eyja.dev/mock-smtp-server:latest \
    python -m aiosmtpd -c mock_smtp_server.RedisSMTPBackend -l 0.0.0.0:8025 -n
