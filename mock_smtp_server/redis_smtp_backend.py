import os
import json
import aioredis

from email.parser import Parser
from email.policy import default


class RedisSMTPBackend:
    def __init__(self):
        self.redis_url = os.environ.get('REDIS_URL')

    async def handle_RCPT(self, server, session, envelope, address, rcpt_options):
        envelope.rcpt_tos.append(address)
        return '250 OK'

    async def handle_DATA(self, server, session, envelope):
        headers = Parser(policy=default).parsestr(envelope.content.decode('utf8'))
        message_id = headers['Message-ID'].replace('<', '').replace('>', '')

        text = None
        html = None

        if headers.is_multipart():
            for part in headers.get_payload():
                if part.get_content_charset() is None:
                    text = part.get_payload(decode=True)
                    continue

                charset = part.get_content_charset()

                if part.get_content_type() == 'text/plain':
                    text = part.get_payload(decode=True).decode()

                if part.get_content_type() == 'text/html':
                    html = part.get_payload(decode=True).decode()

                if text is not None:
                    text = text.strip()
                if html is not None:
                    html = html.strip()
        else:
            text = headers.get_payload(decode=True).decode()

        result = {
            'envelope_from': envelope.mail_from,
            'envelope_to': envelope.rcpt_tos,
            'message': {
                'message_id': message_id,
                'subject': headers['subject'],
                'body': {
                    'text': text,
                    'html': html,
                },
                'from': [
                    {
                        'display_name': address.display_name,
                        'username': address.username,
                        'domain': address.domain,
                    } for address in headers['from'].addresses
                ],
                'to': [
                    {
                        'display_name': address.display_name,
                        'username': address.username,
                        'domain': address.domain,
                    } for address in headers['to'].addresses
                ]
            }
        }

        json_result = json.dumps(result, indent=4)

        redis_connection = aioredis.from_url(self.redis_url)
        async with redis_connection.client() as conn:
            await conn.set(message_id, json_result)

        return '250 Message accepted for delivery'
