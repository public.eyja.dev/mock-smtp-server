FROM python:3.8.12 as stage-1

ENV WORK_DIR '/app'

WORKDIR ${WORK_DIR}

RUN pip3 install -U pip && pip3 install pipenv
COPY Pipfile Pipfile.lock ./
RUN pipenv install --system --deploy

FROM stage-1 as stage-2

EXPOSE 8025

WORKDIR ${WORK_DIR}

COPY mock_smtp_server ./mock_smtp_server